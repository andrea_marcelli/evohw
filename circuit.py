#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
## Author: Andrea Marcelli                                                  ##
## Phd @ Politecnico di Torino                                              ##
## andrea.marcelli@polito.it                                                ##
## 27/04/2017 - Somewhere in Italy                                          ##
## evoHW - Evaluator for combinatorial logic circuits                       ##
## evoHW is distributed under a BSD-style license -- See file LICENSE.md    ##
##############################################################################

import time
from itertools import *
from gate import Gate

def read_file(file):
    """Read the raw content of a file.

    Keyword arguments:
    file -- File to read
    """
    file_content = open(file)
    return file_content.read()

class Circuit(object):
    '''
        Simulate combinational logic circuits.
        Arguments:
        file -- Circuit file to read
    '''
    def __init__(self, file):
        self.__parse_circuit_file(file)

    def get_input_counter(self):
        return self.inputs_dicts_counter+1

    def check_logic_modifier_string(self, array):
        if len(array) != inputs_dicts_counter+1:
            return False
        counter = 0;
        for i in array:
            if i != 0 and i!= 1 and i!=2:
                return False
            if i != 0:
                counter=counter+1
        if counter > (inputs_dicts_counter+1/2):
            return False
        return True

    def __find_available_gate_number(self, currentid):
        return currentid+7000

    def modify_circuit(self, array):
        counter_insertion = 0
        for i in range(len(array)):
            if int(array[i]) == 1 or int(array[i]) == 2:
                gate_number = self.__find_available_gate_number(i)
                new_gate_inputs = ["I{0}".format(self.key_counter)]
                self.inputs_dicts[self.key_counter] = self.inputs_dicts_counter
                self.inputs_dicts_counter = self.inputs_dicts_counter+1
                new_gate_inputs.append(str(self.__gates[i].id))
                self.key_counter += 1
                for this_gate in self.__gates:
                    for input_index in range(len(this_gate.input)):
                        if this_gate.input[input_index] == str(self.__gates[i].id):
                            old_input = this_gate.input[input_index]
                            this_gate.input[input_index] = str(gate_number)

                if int(array[i]) == 1:
                    self.__gates.append(Gate(gate_number, "type1_"+str(gate_number), "xor", new_gate_inputs, 1, self.__gates[i].id+1))
                    counter_insertion+=1
                if int(array[i]) == 2:
                    self.__gates.append(Gate(gate_number, "type2_"+str(gate_number), "xnor", new_gate_inputs, 1, self.__gates[i].id+1))
                    counter_insertion+=1
                self.gates_dicts[gate_number] = self.gates_dicts_counter
                self.gates_dicts_counter = self.gates_dicts_counter +1
        self.__gates.sort(key = lambda x: x.sorting)
        return counter_insertion


    def get_real_index(self, index):
        return self.gates_dicts[index]

    def get_circuit_gate_length(self):
        return len(self.__gates)

    def get_cricuit_output(self):
        ids = set(self.gates_dicts.keys())
        input_ids = set([])
        for gate in self.__gates:
            for input in gate.input:
                if not input.startswith("I"):
                    input_ids.add(int(input))
        output_ids = ids.difference(input_ids)
        return output_ids


    def __parse_circuit_file(self, file):
        '''
            Parse the circuit file.
            Arguments:
            file -- Circuit file to read
        '''
        self.__get_gates_from_file(file)
        self.__gates.sort(key = lambda x: x.id)

    def __get_gates_from_file(self, file):
        '''
            Get all the gates from the circuit file and store it in the circuit.
            Arguments:
            file -- Circuit file to read
        '''
        self.__gates = []
        self.inputs_dicts = {}
        self.inputs_dicts_counter = 0
        self.gates_dicts = {}
        self.gates_dicts_counter = 0
        self.key_counter = 8000
        file_content = read_file(file)
        file_content_lines = file_content.splitlines()
        for i in range(len(file_content_lines)):
            data = file_content_lines[i].split()
            data_fields = []
            gate_inputs = []
            for j in range(len(data)):
                if j == 0:
                    data_fields.append(int(data[j]))
                elif j > 2:
                    gate_inputs.append(data[j].upper())
                else:
                    data_fields.append(data[j].upper())
            data_fields.append(gate_inputs)
            score = 2
            for gate_input in gate_inputs:
                if gate_input.startswith("I"):
                    score -= 1
                    gate_input_row = self.__get_int_of_general_value(gate_input)
                    if gate_input_row not in self.inputs_dicts:
                        self.inputs_dicts[gate_input_row] = self.inputs_dicts_counter
                        self.inputs_dicts_counter = self.inputs_dicts_counter+1
            self.gates_dicts[data_fields[0]] = self.gates_dicts_counter
            self.gates_dicts_counter = self.gates_dicts_counter +1
            data_fields.append(score)
            self.__gates.append(Gate(data_fields[0], data_fields[1], data_fields[2], data_fields[3], data_fields[4], data_fields[0]))

    def print_gates(self):
        '''
            Print the gates in the current circuit sorted by ID.
        '''
        print("ID    Name    Type    Inputs")
        print("-" * 80)
        for gate in self.__gates:
            print(str(gate.id).ljust(2) + " " * 4 + gate.name.ljust(12)
                  + gate.type.ljust(4) + " " * 4, end="")
            for input in gate.input:
                print(input.ljust(2) + "  ", end="")
            print()


    def get_output_for_combination(self, selected_outputs, combination):
        '''
            Print the output of the circuit with the selected outputs
            (if applicable) and for the specified combination.
            Arguments:
            selected_outputs -- List of selected outputs
            selected_outputs -- List of input combination
        '''
        num_general_values = self.__get_num_of_general_input_values()
        if (len(combination) != num_general_values):
            raise Exception('combination input is wrong')
        for i in combination:
            if (int(i) != 1 and int(i) != 0):
                print (i)
                raise Exception('combination input is wrong')
        gate_values = self.__calculate_outputs_for_combinations(combination)
        return gate_values


    def __calculate_outputs_for_combinations(self, combination):
        '''
            Calculate the outputs for the current bit combination.
            Arguments:
            combination -- Current bit combination
        '''
        gate_values = [''] * len(self.__gates)
        while not self.__are_all_gate_values_found(gate_values):
            for i in range(len(self.__gates)):
                if gate_values[self.gates_dicts[self.__gates[i].id]] != '':
                    continue
                if self.__are_all_required_inputs_available(self.__gates[i], gate_values):
                    inputs = []
                    for input in self.__gates[i].input:
                        if input.startswith("I"):
                            inputs.append(int(combination[self.inputs_dicts[self.__get_int_of_general_value(input)]]))
                        else:
                            inputs.append(int(gate_values[self.gates_dicts[int(input)]]))
                    gate_values[self.gates_dicts[self.__gates[i].id]] = self.__gates[i].logic_output(inputs)
        return gate_values


    def get_output_probabilities(self):
        '''
            Print the probabilities of the selected outputs (if applicable).
            If no outputs are selected, then all gate probabilities will be printed.
            Arguments:
            selected_outputs -- List of selected outputs
        '''
        num_input_values = self.__get_num_of_general_input_values()
        input_probabilities = ['0.5'] * num_input_values
        gate_probabilities = self.__calculate_output_probabilities(input_probabilities)
        return gate_probabilities


    def __calculate_output_probabilities(self, input_probabilities):
        '''
            Calculate the outputs probabilities for each gate.
            Arguments:
            input_probabilities -- Probability in input to the circuit
        '''
        gate_values = [''] * len(self.__gates)
        while not self.__are_all_gate_values_found(gate_values):
            for i in range(len(self.__gates)):
                if gate_values[self.gates_dicts[self.__gates[i].id]] != '':
                    continue
                if self.__are_all_required_inputs_available(self.__gates[i], gate_values):
                    inputs = []
                    for input in self.__gates[i].input:
                        if input.startswith("I"):
                            inputs.append(float(input_probabilities[self.inputs_dicts[self.__get_int_of_general_value(input)]]))
                        else:
                            inputs.append(float(gate_values[self.gates_dicts[int(input)]]))
                    gate_values[self.gates_dicts[self.__gates[i].id]] = self.__gates[i].output_probability(inputs)
        return gate_values

    def __print_gate_outputs(self, combination, selected_outputs, gate_values):
        '''
            Print the outputs of the selected gates in the truth table.
            Arguments:
            selected_outputs -- List of selected outputs
            gate_values      -- List of values for each gate
        '''
        for bit in combination:
            print(str(bit).ljust(2) + " " * 4, end="")
        if len(selected_outputs) > 0:
            for output in selected_outputs:
                print(str(gate_values[int(output)]).ljust(8), end="")
        else:
            for gate in self.__gates:
                print(str(gate_values[self.gates_dicts[gate.id]]).ljust(8), end="")
        print()

    def get_inputs_number(self):
        return self.__get_num_of_general_input_values()

    def __get_num_of_general_input_values(self):
        '''
            Get the number of general input values.
            Arguments:
            <None>
        '''
        num_general_values = 0
        general_values = []
        for gate in self.__gates:
            for input in gate.input:
                if input.startswith("I"):
                    general_value = self.__get_int_of_general_value(input)
                    inserisco = True
                    for i in general_values:
                        if i == general_value:
                            inserisco = False
                    if inserisco == True:
                        general_values.append(general_value)
        return len(general_values)

    def __get_int_of_general_value(self, general_value):
        '''
            Get the integer component of the general value formatted I0, I1, etc.
            Arguments:
            general_value -- General value formatted I0, I1, etc.
        '''
        general_values = general_value.split("I")
        return int(general_values[1])

    def __are_all_gate_values_found(self, gate_values):
        '''
            Check if all the gate values are found.
            This is using a developer-chosen definition of empty being ''.
            Arguments:
            gate_values -- List of gate values
        '''
        for value in gate_values:
            if value == '':
                return False
        return True

    def __are_all_required_inputs_available(self, gate, gate_values):
        '''
            Check if all the required inputs for the current gate are available.
            Arguments:
            gate        -- Current logic gate
            combination -- List of general input values
            gate_values -- List of gate values
        '''
        for input in gate.input:
            if not input.startswith("I"):
                if gate_values[self.gates_dicts[int(input)]] == '':
                    return False
        return True
