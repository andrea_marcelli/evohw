                     _    ___          __
                    | |  | \ \        / /
      _____   _____ | |__| |\ \  /\  / /
     / _ \ \ / / _ \|  __  | \ \/  \/ /  
    |  __/\ V / (_) | |  | |  \  /\  /   
     \___| \_/ \___/|_|  |_|   \/  \/    
      by Andrea Marcelli

usage: evoHW.py [-h] -i UGPINPUT -c CIRCUIT -p PREFIX [-mc MAXCOMBINATIONS]

evoHW.py: error: the following arguments are required: -i/--ugpinput, -c/--circuit, -p/--prefix

Examples:

./evoHW.py -i ./iscas85/c17_ugp_input.txt -c ./iscas85/c17.in -p ./iscas85/c17_prefix.txt

./evoHW.py -i ./iscas85/c499_ugp_input.txt -c ./iscas85/c499.in -p ./iscas85/c499_prefix.txt -mc 10

./evoHW.py -i ./iscas85/c2670_ugp_input.txt -c ./iscas85/c2670.in -p ./iscas85/c2670_prefix.txt -mc 4
