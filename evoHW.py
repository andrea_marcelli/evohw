#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
## Author: Andrea Marcelli                                                  ##
## Phd @ Politecnico di Torino                                              ##
## andrea.marcelli@polito.it                                                ##
## 27/04/2017 - Somewhere in Italy                                          ##
## evoHW - Evaluator for combinatorial logic circuits                       ##
## evoHW is distributed under a BSD-style license -- See file LICENSE.md    ##
##############################################################################

'''
    Note:
    New logic ports are added at the end of the gate list, with a high id (>8000).
    New inputs (keys) are added at the end of input list, with high id (>7000).
'''

import os.path
import argparse
import sys

import time
import random
import logging

import numpy as np

from gate import Gate
from circuit import Circuit

assert sys.version_info >= (3, 4)

def get_new_gate_length(ugp_input, circuit_length):
    new_gate_length = 0
    for i in list(map(int, ugp_input)):
        if i == 1 or i == 2:
            new_gate_length += 1
    return new_gate_length


def get_number_rare_signals(probabilities, threshold):
    number_rare_signals = 0
    for i in probabilities:
        if float(i) < threshold or float(i) > (1-threshold):
            number_rare_signals+=1
    return number_rare_signals


def check_correctness_alteratioin(ugp_input, circuit_length):
    correctness = True
    if len(ugp_input) != circuit_length:
        correctness = False
        logging.critical("It should be {1}, but is {0}".format(len(ugp_input), circuit_length))
    return correctness


def get_safe_reverse(input):
    if input == 0:
        output = 999999999
    else:
        output = 1/input
    return output


def get_output_from_evaluation(circuit, base_circuit_outputs, evaluation):
    global circuit_outputs
    if len(circuit_outputs) == 0:
        for i in base_circuit_outputs:
            circuit_outputs.append(circuit.get_real_index(i))
    result = [evaluation[i] for i in circuit_outputs]
    return result


def get_hamming(new_evaluation, base_evaluation, base_circuit_outputs, circuit):
    base_evaluation_filtered = get_output_from_evaluation(circuit, base_circuit_outputs, base_evaluation)
    new_evaluation_filtered = get_output_from_evaluation(circuit, base_circuit_outputs, new_evaluation)
    if (len(base_evaluation_filtered) != len(base_circuit_outputs)) or (len(new_evaluation_filtered) != len(base_circuit_outputs)):
        logging.critical("something bad happened!")
        sys.exit()
    result = [base_evaluation_filtered[i] ^ new_evaluation_filtered[i] for i in range(len(base_evaluation_filtered))]
    return sum(result)


def get_mean(array):
    exp_mean = np.mean(np.asarray(array, dtype=float))
    return exp_mean


def get_exp_mean(array):
    exp_mean = np.mean(np.exp(np.absolute(np.asarray(array, dtype=float)-0.5)))
    return exp_mean


def get_dev_std(array):
    std = np.std(np.array(array))
    return std


def get_circuit_gate_length(circuit):
    gate_length = circuit.get_circuit_gate_length()
    return gate_length


def get_cricuit_output(circuit):
    outputs = circuit.get_cricuit_output()
    return outputs


def get_circuit_input_length(circuit):
    inputs_number = circuit.get_inputs_number()
    return inputs_number


def alter_circuit(ugp_input, circuit):
    number_new_gates = circuit.modify_circuit(ugp_input)
    return number_new_gates


def evaluate_signal_probabilities(circuit):
    probs = circuit.get_output_probabilities()
    return probs


def evaluate_circuit(input_combination_array, circuit):
    evaluation_output = circuit.get_output_for_combination("", input_combination_array)
    return evaluation_output


def recursive(circuit, prefix_array, step, input_combination_array, length_of_combination, all_combinations, base_evaluation, base_circuit_outputs):
    global hamming_distances
    if step == length_of_combination:
        if all_combinations == True or (all_combinations == False and random.randint(0, 1) == 1):
            combination_array = prefix_array[:]
            combination_array.extend(input_combination_array)
            new_evaluation = evaluate_circuit(combination_array, circuit)
            hd = get_hamming(new_evaluation, base_evaluation, base_circuit_outputs, circuit)
            hamming_distances.append(hd)
        return
    input_combination_array.append(0)
    recursive(circuit, prefix_array, step+1, input_combination_array, length_of_combination, all_combinations, base_evaluation, base_circuit_outputs)
    input_combination_array.pop()
    input_combination_array.append(1)
    recursive(circuit, prefix_array, step+1, input_combination_array, length_of_combination, all_combinations, base_evaluation, base_circuit_outputs)
    input_combination_array.pop()


def main():
    """ That's main! """

    parser = argparse.ArgumentParser(description="evoHW ** a combinatorial logic evaluator for uGP integration")
    parser.add_argument('-i', '--ugpinput', required=True, help='Input from uGP')
    parser.add_argument('-c', '--circuit', required=True, help='Combinatorial circuit to evaluate')
    parser.add_argument('-p', '--prefix', required=True, help='Combinatorial inputs')
    parser.add_argument('-mc', '--maxcombinations', required=False, help='Maximum number of combinations to evaluate')
    args = parser.parse_args()

    global hamming_distances

    file_ugp_input = args.ugpinput
    file_circuit = args.circuit
    file_prefix_combination = args.prefix

    if args.maxcombinations:
        all_combinations = False
        max_combinations = int(args.maxcombinations)
    else:
        all_combinations = True

    logging.info("Input ugp: {}".format(file_ugp_input))
    logging.info("Circuit: {}".format(file_circuit))
    logging.info("File prefix combination: {}".format(file_prefix_combination))
    logging.info("All combinations: {0}".format(all_combinations))
    if args.maxcombinations:
        logging.info("Max combinations: {0}".format(max_combinations))

    if os.path.isfile(file_ugp_input) and os.path.isfile(file_circuit) and os.path.isfile(file_prefix_combination):
        with open(file_prefix_combination, "r") as prefix_in:
            with open(file_ugp_input, "r") as f_in:
                logging.info("Building circuit...")
                base_circuit = Circuit(file_circuit)
                base_circuit_input_length = get_circuit_input_length(base_circuit)
                base_circuit_gate_lenght = get_circuit_gate_length(base_circuit)
                base_circuit_outputs = get_cricuit_output(base_circuit)
                ugp_input = f_in.readline().split();
                if check_correctness_alteratioin(ugp_input, base_circuit_gate_lenght) == False:
                    logging.critical("uGP Input is not correct")
                    sys.exit()
                new_circuit = Circuit(file_circuit)
                number_new_gates = alter_circuit(ugp_input, new_circuit)
                exp_mean_hds_array = []
                mean_hds_array = []

                logging.info("Evaluating circuit...")
                for prefix in prefix_in.readlines():
                    prefix_array = prefix.split()
                    if len(prefix_array) != base_circuit_input_length:
                        logging.critical("length is: " + str(len(prefix_array)) + " but should be " + str(base_circuit_input_length))
                        sys.exit()
                    base_circuit_evaluation = evaluate_circuit(prefix_array, base_circuit)
                    if all_combinations == True:
                        recursive(new_circuit, prefix_array, 0, input_combination_array, number_new_gates, all_combinations, base_circuit_evaluation, base_circuit_outputs)
                    else:
                        for i in range(max_combinations):
                            combination_array = prefix_array[:]
                            combination_array.extend([random.randint(0,1) for i in range(number_new_gates)])
                            new_evaluation = evaluate_circuit(combination_array, new_circuit)
                            hd = get_hamming(new_evaluation, base_circuit_evaluation, base_circuit_outputs, new_circuit)
                            hamming_distances.append(hd)

                    hamming_distances = [i/len(base_circuit_outputs) for i in hamming_distances]
                    exp_mean_hd = get_exp_mean(hamming_distances)
                    exp_mean_hds_array.append(exp_mean_hd)

                    mean_hd = get_mean(hamming_distances)
                    mean_hds_array.append(mean_hd)
                    hamming_distances.clear()
                    input_combination_array.clear()

                logging.info("Evaluating finished...")

                exp_mean_hds = get_exp_mean(exp_mean_hds_array)
                logging.info("Hamming Distance exponential mean: %f", exp_mean_hds)
                mean_hds = get_mean(mean_hds_array)
                logging.info("Hamming Distance mean: %f", mean_hds)

                ## Evaluate the signal prob on BASE CIRCUIT
                base_probs = evaluate_signal_probabilities(base_circuit)
                logging.debug("Signals base probabilities: " + str(base_probs))
                number_old_rare_signals_20 = get_number_rare_signals(base_probs, 0.20)
                logging.info("Number of rare signal (p = 0.20): %d", number_old_rare_signals_20)
                number_old_rare_signals_01 = get_number_rare_signals(base_probs, 0.01)
                logging.info("Number of rare signal (p = 0.01): %d", number_old_rare_signals_01)
                exp_mean_base_probs = get_exp_mean(base_probs)
                logging.info("Signals base exp mean: %f", exp_mean_base_probs)

                ## Evaluate the signal probabilities of NEW CIRCUIT
                new_probs = evaluate_signal_probabilities(new_circuit)
                new_probs_cutted = new_probs[:]
                logging.debug("Signals new probabilities: " + str(new_probs_cutted))
                exp_mean_new_probs_cutted = get_exp_mean(new_probs_cutted)
                logging.debug("Signals new exp mean prob: " + str(exp_mean_new_probs_cutted))
                number_new_rare_signals_20 = get_number_rare_signals(new_probs_cutted, 0.20)
                logging.info("New number of rare signal (p = 0.20): %d", number_new_rare_signals_20)
                number_new_rare_signals_01 = get_number_rare_signals(new_probs_cutted, 0.01)
                logging.info("New number of rare signal (p = 0.01): %d", number_new_rare_signals_01)
                mean_new_probs_cutted = get_mean(new_probs_cutted)
                new_gate_length = get_new_gate_length(ugp_input, base_circuit_input_length)
                logging.info("Original circuit length: %d", base_circuit_input_length)
                logging.info("Circuit input length: %d", new_gate_length)

                ## Print fitness information
                first_fitness = get_safe_reverse(new_gate_length)
                logging.info("First fitness: %f", first_fitness)
                second_fitness = get_safe_reverse(exp_mean_new_probs_cutted)
                logging.info("second fitness: %f", second_fitness)
                third_fitness = get_safe_reverse(exp_mean_hds)
                logging.info("Third fitness: %f", third_fitness)

                print("\n{} {} {}".format(str(first_fitness), str(second_fitness), str(third_fitness)))
    else:
        logging.critical("Cannot find file at the specified path")
        sys.exit()

circuit_outputs = []
input_combination_array = []
hamming_distances = []

if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s %(levelname)s:: %(message)s", datefmt="%H:%M:%S")
    logging.getLogger().setLevel(level=logging.INFO)
    logging.getLogger().handlers[0].setFormatter(logging.Formatter("%(message)s"))
    logging.warning(r'                     _    ___          __')
    logging.warning(r'                    | |  | \ \        / /')
    logging.warning(r'      _____   _____ | |__| |\ \  /\  / / ')
    logging.warning(r'     / _ \ \ / / _ \|  __  | \ \/  \/ /  ')
    logging.warning(r'    |  __/\ V / (_) | |  | |  \  /\  /   ')
    logging.warning(r'     \___| \_/ \___/|_|  |_|   \/  \/    ')
    logging.warning(r'      by Andrea Marcelli')
    logging.warning(r'')
    logging.getLogger().handlers[0].setFormatter(logging.Formatter("%(asctime)s.%(msecs)04d %(levelname)s:: %(message)s", datefmt="%H:%M:%S"))

    try:
        main()
    except Exception as e:
    	print("Some error occured!")
    	print(str(e))
