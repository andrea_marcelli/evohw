#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
## Author: Andrea Marcelli                                                  ##
## Phd @ Politecnico di Torino                                              ##
## andrea.marcelli@polito.it                                                ##
## 27/04/2017 - Somewhere in Italy                                          ##
## evoHW - Evaluator for combinatorial logic circuits                       ##
## evoHW is distributed under a BSD-style license -- See file LICENSE.md    ##
##############################################################################

import logging
from itertools import *

class Gate(object):
    ''' Simulate logic gates.'''

    def __init__(self, id, name, type, input, score, sorting):
        '''
            id    -- Block ID
            name  -- Block name
            type  -- Logic gate type
            input -- List of input names
        '''
        self.id = id
        self.name = name.upper()
        self.type = type.upper()
        self.input = input
        self.score = score
        self.sorting = sorting

    def logic_output(self, input):
        '''
            Evaluate the output of the current gate based on its type.
            input -- Input value; could be a list (all) or a single int (NOT gate)
        '''
        if self.type == "NOT":
            return self.__not(input)
        elif self.type == "BUF":
            return self.__buf(input)
        elif self.type == "OR":
            return self.__or(input)
        elif self.type == "AND":
            return self.__and(input)
        elif self.type == "XOR":
            return self.__xor(input)
        elif self.type == "NAND":
            return self.__not(self.__and(input))
        elif self.type == "NOR":
            return self.__not(self.__or(input))
        elif self.type == "XNOR":
            return self.__not(self.__xor(input))
        else:
            print("ERROR:: Invalid gate type (type = \"" + self.type + "\")")
            print("        Use a valid gate type (NOT, OR, AND, XOR, NAND, NOR, or XNOR).")
            return 0

    def output_probability(self, input):
        '''
            Evaluate the output probability of the current gate based on its type.
            input -- Input value; could be a list (all) or a single int (NOT gate)
            Note: the probabilities are inteneded as the probabilities of the input being one
        '''
        if self.type == "NOT":
            probab = self.__not_probability(input)
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "BUF":
            probab = self.__buf_probability(input)
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "OR":
            probab = self.__or_probability(input)
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "AND":
            probab = self.__and_probability(input)
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "XOR":
            probab = self.__xor_probability(input)
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "NAND":
            probab = self.__not_probability(self.__and_probability(input))
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "NOR":
            probab = self.__not_probability(self.__or_probability(input))
            logging.debug("probability: %f", probab)
            return probab
        elif self.type == "XNOR":
            probab =  self.__not_probability(self.__xor_probability(input))
            logging.debug("probability: %f", probab)
            return probab
        else:
            logging.critical("Invalid gate type (type ='%s')", self.type)
            logging.info("Valid gate types: NOT, OR, AND, XOR, NAND, NOR and XNOR")
            return 0

    def truth_table(self, bit_size):
        '''
            Print out the truth table of the logic gate.
            This is designed primarily for verifygateclass.py and debugging
            purposes to verify each logic implementation.
            Arguments:
            bit_size -- Maximum number of bits to test
        '''
        if self.type == "NOT":
            print("I0 OUT")
            for i in range(2):
                print(str(i).rjust(2) + " ", end="")
                print(str(self.logic_output(i)).rjust(2))
        else:
            for i in range(bit_size):
                print("I" + str(i) + " ", end="")
            print("OUT")
            combinations = list(product([0, 1], repeat=bit_size))
            for combination in combinations:
                for bit in combination:
                    print(str(bit).rjust(2) + " ", end="")
                print(str(self.logic_output(combination)).rjust(2))

    def print_info(self):
        '''
            Print the stored gate information.
            This is designed primarily for verifygateclass.py and debugging
            purposes to verify the accuracy of each logic implementation.
        '''
        print("Gate " + str(self.id))
        print("    * Name   : " + self.name)
        print("    * Type   : " + self.type)
        print("    * Inputs : " + str(self.input))

    def __not(self, input):
        '''
            Perform a logic NOT on the input value.
            Arguments:
            input -- Input value
        '''
        final_input = None
        if type(input) is list:
            if (len(input) > 1 ):
                raise Exception('not_prob input > 1 not supported')
            final_input = input[0]
        else:
            final_input = input
        if final_input is None:
            print("ERROR:: Invalid input to NOT gate (input = \"" + input + "\"")
        else:
            if final_input is 1:
                return 0
            else:
                return 1

    def __not_probability(self, input):
        '''
            Calculate the probability of a logic NOT on the input value.
            Arguments:
            input -- Input value
        '''
        final_input = None
        if type(input) is list:
            if (len(input) > 1 ):
                raise Exception('not_prob input > 1 not supported')
            final_input = input[0]
        else:
            final_input = input
        if final_input is None:
            print("ERROR:: Invalid input to NOT gate (input = \"" + input + "\"")
        else:
            return 1-final_input;

    def __buf(self, input):
        '''
            Perform a logic BUF on the input value.
            Arguments:
            input -- Input value
        '''
        final_input = None
        if type(input) is list:
            if (len(input) > 1 ):
                raise Exception('not_prob input > 1 not supported')
            final_input = input[0]
        else:
            final_input = input
        if final_input is None:
            print("ERROR:: Invalid input to NOT gate (input = \"" + input + "\"")
        else:
            return final_input

    def __buf_probability(self, input):
        '''
            Calculate the probability of a logic BUF on the input value.
            Arguments:
            input -- Input value
        '''
        final_input = None
        if type(input) is list:
            if (len(input) > 1 ):
                raise Exception('not_prob input > 1 not supported')
            final_input = input[0]
        else:
            final_input = input
        if final_input is None:
            print("ERROR:: Invalid input to NOT gate (input = \"" + input + "\"")
        else:
            return final_input

    def __and(self, input):
        '''Perform a logic AND on all the input values.

        Arguments:
        input -- List of input values
        '''
        for value in input:
            if value is 0:
                return 0
        return 1

    def __and_probability(self, input):
        '''
            Calculate the probability of a logic AND on all the input values.
            Arguments:
            input -- List of input values
        '''
        final_value = 1
        for value in input:
            final_value *= float(value);
        return final_value

    def __or(self, input):
        '''
            Perform a logic OR on all the input values.
            Arguments:
            input -- List of input values
        '''
        for value in input:
            if value is 1:
                return 1
        return 0

    def __or_probability(self, input):
        '''
            Calculate the probbility of a logic OR on all the input values.
            Arguments:
            input -- List of input values
        '''
        temp_value = 1
        for value in input:
            temp_value *= (1 - float(value))
        return 1 - temp_value

    def __xor(self, input):
        '''
            Perform a logic XOR on all the input values.
            Arguments:
            input -- List of input values
        '''
        value = input[0]
        for i in range(1, len(input)):
            if value is input[i]:
                value = 0
            else:
                value = 1
        return value

    def __xor_probability(self, input):
        '''
            Calculate the probability a logic XOR on all the input values.
            Arguments:
            input -- List of input values
        '''
        if (len(input) > 2 ):
            raise Exception('xor_prob input > 2 not supported')
        temp_value = 1
        final_value = 0
        for i in input:
            final_value += float(i)
            temp_value *= float(i)
        return final_value - 2 * temp_value
